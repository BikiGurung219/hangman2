package controller;

import apptemplate.AppTemplate;
import data.GameData;
import gui.Workspace;
import javafx.animation.AnimationTimer;
import javafx.application.Platform;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import propertymanager.PropertyManager;
import ui.AppMessageDialogSingleton;
import ui.YesNoCancelDialogSingleton;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;


import static settings.AppPropertyType.*;
import static settings.InitializationParameters.APP_WORKDIR_PATH;

/**
 * @author Ritwik Banerjee
 */
public class HangmanController implements FileController {


    public enum GameState {
        UNINITIALIZED,
        INITIALIZED_UNMODIFIED,
        INITIALIZED_MODIFIED,
        ENDED
    }

    private AppTemplate appTemplate; // shared reference to the application
    private GameData    gamedata;    // shared reference to the game being played, loaded or saved
    private GameState   gamestate;   // the state of the game being shown in the workspace
    private Text[]      progress;    // reference to the text area for the word
    private boolean     success;     // whether or not player was successful
    private int         discovered;  // the number of letters already discovered
    private Button      gameButton;  // shared reference to the "start game" button
    private Label       remains;     // dynamically updated label that indicates the number of remaining guesses
    private Path workFile;
    private GridPane  guessLetterPane;
    private int goodGuess = 1;
    private int badGuess = 1;
    private Label difficultyLevelString;
    private char getWord;
    boolean hintChecker;
    public static Text[] gameGoodGuess;
    public GraphicsContext gc;
    boolean hintButtonStatus = false;
    boolean flagLoadSave = false;       // The flag for notification of the chnages in the load file




    public HangmanController(AppTemplate appTemplate, Button gameButton) {
        this(appTemplate);
        this.gameButton = gameButton;
    }

    public HangmanController(AppTemplate appTemplate) {
        this.appTemplate = appTemplate;
        this.gamestate = GameState.UNINITIALIZED;
        guessLetterPane = new GridPane();
    }

    public void enableGameButton() {
        if (gameButton == null) {
            Workspace workspace = (Workspace) appTemplate.getWorkspaceComponent();
            gameButton = workspace.getStartGame();
        }
        gameButton.setDisable(false);
    }

    public void disableGameButton() {
        if (gameButton == null) {
            Workspace workspace = (Workspace) appTemplate.getWorkspaceComponent();
            gameButton = workspace.getStartGame();
        }
        gameButton.setDisable(true);
    }

    public void setGameState(GameState gamestate) {
        this.gamestate = gamestate;
    }

    public GameState getGamestate() {
        return this.gamestate;
    }

    /**
     * In the homework code given to you, we had the line
     * gamedata = new GameData(appTemplate, true);
     * This meant that the 'gamedata' variable had access to the app, but the data component of the app was still
     * the empty game data! What we need is to change this so that our 'gamedata' refers to the data component of
     * the app, instead of being a new object of type GameData. There are several ways of doing this. One of which
     * is to write (and use) the GameData#init() method.
     */
    public void start() {
        gamedata = (GameData) appTemplate.getDataComponent();
        guessLetterPane= new GridPane();
        success = false;
        discovered = 0;
        goodGuess = 1;
        badGuess = 1;
        hintButtonStatus = false;
        Workspace gameWorkspace = (Workspace) appTemplate.getWorkspaceComponent();
        gamedata.init();
        setGameState(GameState.INITIALIZED_UNMODIFIED);
        HBox remainingGuessBox = gameWorkspace.getRemainingGuessBox();
        VBox guessedLetters    = (VBox) gameWorkspace.getGameTextsPane().getChildren().get(1);
        remains = new Label(Integer.toString(GameData.TOTAL_NUMBER_OF_GUESSES_ALLOWED));
        remainingGuessBox.getChildren().addAll(new Label("Remaining Guesses: "), remains);
        VBox gameTextPaneDemo= (VBox) gameWorkspace.getGameTextsPane();
        GridPane here=gameWorkspace.generateLetterHeadings();       // The Gridpane to add the A-Z pane in workspace
        gameTextPaneDemo.getChildren().addAll(here);         // adding the gridpane to the gametextPane in workspace
        gc= gameWorkspace.getCanvas();
        initWordGraphics(guessedLetters );

        play();
    }

    /**
     * This method helps to put the Hangman graphics in specific area with the number of guess that has been declared by the user
     * The switch contitional loop helps to find the number of hangman shapes to show in the pane
     * @Author BG
     * @param numBadGuess
     */


    public void makeHangmanGraphics(int numBadGuess)  {
        gc.setStroke(Color.BLUE);
        gc.setLineWidth(5);
        int numberRefGraph = 0;
        for(int i =0;i<(numBadGuess-1);i++) {
            numberRefGraph = i+1;
            switch (numberRefGraph){
                case 1:
                    gc.strokeLine(10, 10, 10, 300);     //g1
                    break;
                case 2:
                    gc.strokeLine(10, 10, 150, 10);     //g2
                    break;
                case 3:
                    gc.strokeLine(150, 10, 150, 70);    //g3
                    break;
                case 4:
                    gc.strokeLine(10,300,250,300);      //g4
                    break;
                case 5:
                    // for Head                         //g5
                    String headColor = "black";
                    String outlineColor = "#000000";
                    int headW = 60;
                    int headH = 60;
                    gc.setFill(Paint.valueOf(headColor));
                    gc.fillOval(125, 70, headW, headH);
                    gc.beginPath();
                    gc.setStroke(Paint.valueOf(outlineColor));
                    gc.setLineWidth(1);
                    gc.strokeOval(125 , 70, headW, headH);
                    gc.stroke();
                    break;
                case 6:
                    gc.setStroke(Color.BLACK);
                    gc.setLineWidth(5);
                    gc.strokeLine(150,130 ,150,184);    //g6
                    break;
                case 7:
                    gc.strokeLine(150,135,125,165);     //g7
                    break;
                case 8:
                    gc.strokeLine(150,135,175,165);     //g8
                    break;
                case 9:
                    gc.strokeLine(150,185,125,215);     //g9
                    break;
                case 10:
                    gc.strokeLine(150,185,175,215);     //g10
                    break;
                default:
                    System.out.println(" Restart the Game. ");
                    break;
            }
        }
    }

    private void end() {
        appTemplate.getGUI().getPrimaryScene().setOnKeyTyped(e-> {
            // do nothing because the game is already been over//
            PropertyManager           manager    = PropertyManager.getManager();
            AppMessageDialogSingleton dialog     = AppMessageDialogSingleton.getSingleton();
            dialog.show(manager.getPropertyValue(GAME_OVER_TITLE), "Game Over!! Please Start a New Game!!");
        });
        gameButton.setDisable(true);
        appTemplate.getGUI().enableHintButton(true);
        setGameState(GameState.ENDED);
        appTemplate.getGUI().updateWorkspaceToolbar(gamestate.equals(GameState.INITIALIZED_MODIFIED));
        Platform.runLater(() -> {
            PropertyManager           manager    = PropertyManager.getManager();
            AppMessageDialogSingleton dialog     = AppMessageDialogSingleton.getSingleton();
            String                    endMessage = manager.getPropertyValue(success ? GAME_WON_MESSAGE : GAME_LOST_MESSAGE);

            try{
                if(success){
                    dialog.show(manager.getPropertyValue(GAME_OVER_TITLE), endMessage);
                }else{
                    dialog.show(manager.getPropertyValue(GAME_OVER_TITLE), endMessage);
                }
            }catch (Exception e){
                // do nothing it shows some show and wait Exceptions which has already been handled by the user
               // System.out.println("The same game has been loaded");
            }

            if (dialog.isShowing())
                dialog.toFront();
            else{
                printRemainGuess();
            }

        });
    }


    /**
     * This method helps to print the final words that were not guessed by the user.
     * It changes the color of the Characters that were not guessed and helps user know that the game is over.
     * @Author BG
     */
    public void printRemainGuess(){
        for(int i=0;i<progress.length;i++){
            if(gamedata.getGoodGuesses().contains(progress[i].getText().charAt(0))){
                //do nothing
            }
            else{
                progress[i].setFill(Color.RED);
                progress[i].setVisible(true);
            }
        }
    }

    public void handleHintRequest(){
        hintChecker= gamedata.hintUsedChecking(true);
        getWord=gamedata.checkIfGoodGuess();
        displayHintWord(getWord);
        setGameState(GameState.INITIALIZED_MODIFIED);
        hintButtonStatus = true;
        appTemplate.getGUI().enableHintButton(hintButtonStatus);
    }

    public void displayHintWord(char ch) {
        boolean displayCorrectGuess = true;
        Workspace gameWorkspace = (Workspace) appTemplate.getWorkspaceComponent();
        for (int i = 0; i < progress.length; i++) {
            if (ch == progress[i].getText().charAt(0)) {
                progress[i].setVisible(true);
                gamedata.addBadGuess(ch);
                gamedata.addGoodGuess(ch);
                discovered++;
                goodGuess++;
                if(displayCorrectGuess){
                    gameWorkspace.changeColorIfWrong(ch);
                    Text tt= new Text(ch+", ");
                    guessLetterPane.add(tt,(goodGuess-1),0);
                    badGuess++;
                    makeHangmanGraphics(badGuess);
                    remains.setText(Integer.toString(gamedata.getRemainingGuesses()));
                    displayCorrectGuess = false;

                }
                else{
                    // do nothing
                }
            }
        }
        if(success = (progress.length == discovered)){
         end();
        }
    }


    public void checkingDifficultyHint(){
        String randomWordHint= gamedata.checkDifficulty();
        if(randomWordHint.equals("Difficult")){
            appTemplate.getGUI().showHintButton();
            difficultyLevelString = new Label(randomWordHint);
            if(!hintButtonStatus){
               appTemplate.getGUI().enableHintButton(false);
            }
        }
        else{
            appTemplate.getGUI().hideHintButton();
            difficultyLevelString= new Label(randomWordHint);
        }
    }


    private void initWordGraphics(VBox guessedLetters)  {

        char[] targetword = gamedata.getTargetWord().toCharArray();
        checkingDifficultyHint();

        // appTemplate.getGUI().enableHintButton(true);
        progress = new Text[targetword.length];


        GridPane pane= new GridPane();
        pane.setGridLinesVisible(true);
        pane.setStyle("-fx-padding:10; -fx-font-size: 25px");
        pane.setHgap(5);

        for (int i = 0; i < progress.length; i++) {
            StackPane paneHolder = new StackPane();
            paneHolder.setStyle("-fx-background-color: transparent");
            progress[i] = new Text(Character.toString(targetword[i]));
            Rectangle rect= new Rectangle(30,30);
            rect.setStyle("-fx-background-color: transparent");
            rect.setFill(Color.valueOf("WHITE"));
            paneHolder.getChildren().addAll(rect, progress[i]);
            progress[i].setVisible(false);
            pane.add(paneHolder, i , 0);

        }

        guessLetterPane.setStyle(" -fx-padding:15px; -fx-font-size: 20px");
        Label correctLabel= new Label("   Correct Guess : ");
        Label wrongLabel = new Label("   Wrong Guess : ");
        Label difficultyLevel= new Label("   Level : ");
        HBox boxDifficultyLabel= new HBox(difficultyLevel,difficultyLevelString);
        guessLetterPane.add(correctLabel, 0 , 0);
        guessLetterPane.add(wrongLabel, 0 , 1);
        guessLetterPane.add(boxDifficultyLabel,0,4);
        guessedLetters.getChildren().addAll(pane, guessLetterPane);
    }

    public void play() {
        disableGameButton();

        AnimationTimer timer = new AnimationTimer() {
            @Override
            public void handle(long now) {
                appTemplate.getGUI().updateWorkspaceToolbar(gamestate.equals(GameState.INITIALIZED_MODIFIED));
                Workspace gameWorkspace = (Workspace) appTemplate.getWorkspaceComponent();
                appTemplate.getGUI().getPrimaryScene().setOnKeyTyped((KeyEvent event) -> {
                     char guess = Character.toLowerCase(event.getCharacter().charAt(0));    // Need to handle some more data handling
                     if(guess >='a' && guess <='z'){        // validate user input to only alphabets
                         if (!alreadyGuessed(guess)) {
                             boolean goodGuessFlag = false;
                             flagLoadSave = false;
                             boolean flag = true;
                             for (int i = 0; i < progress.length; i++) {
                                 try {
                                     if (gamedata.getTargetWord().charAt(i) == guess) {
                                         if (flag) {
                                             gameWorkspace.changeColorIfGuess(guess);
                                             Text t = new Text(guess + ", ");
                                             guessLetterPane.add(t, goodGuess, 0); // adding the good guess simultaneoulsy to pane
                                             goodGuess++;
                                             flag = false;
                                         }

                                         progress[i].setVisible(true);
                                         gamedata.addGoodGuess(guess);
                                         goodGuessFlag = true;
                                         discovered++;
                                     }
                                 }catch (Exception e){
                                     // do nothing
                                 }
                             }
                             if (!goodGuessFlag) {

                                 if(flag){
                                     Text t= new Text(guess+", ");
                                     gameWorkspace.changeColorIfWrong(guess);
                                     guessLetterPane.add(t,badGuess, 1); // adding the bad guess simultaneoulsy to pane
                                     badGuess++;
                                     makeHangmanGraphics(badGuess);
                                 }
                                 gamedata.addBadGuess(guess);
                             }
                             success = (discovered == progress.length);
                             remains.setText(Integer.toString(gamedata.getRemainingGuesses()));
                         }
                     }
                     else{
                         AppMessageDialogSingleton wrongInput= AppMessageDialogSingleton.getSingleton();
                         wrongInput.show("Wrong Input", "Please only enter alphabets from 'a' to 'z'");
                     }
                     if(!flagLoadSave){         // Only can save the file if the file has been changed after loading
                         setGameState(GameState.INITIALIZED_MODIFIED);
                     }

                });
                if (gamedata.getRemainingGuesses() <= 0 || success)
                    stop();
            }

            @Override
            public void stop() {
                super.stop();
                end();
            }
        };
        timer.start();
    }

    private void restoreGUI() {
        disableGameButton();
        Workspace gameWorkspace = (Workspace) appTemplate.getWorkspaceComponent();
        gameWorkspace.reinitialize();
        VBox gameTextPaneDemo= (VBox) gameWorkspace.getGameTextsPane();
        GridPane here=gameWorkspace.generateLetterHeadings();       // The Gridpane to add the A-Z pane in workspace
        gameTextPaneDemo.getChildren().addAll(here);         // Adding the Gridpane to the gameTextPane from the Wrokspace
        gc= gameWorkspace.getCanvas();
        goodGuess = 1;
        badGuess = 1;

        VBox guessedLetters = (VBox) gameWorkspace.getGameTextsPane().getChildren().get(1);
        try{
            restoreWordGraphics(guessedLetters);

            if(gamedata.getHintUsedCheck() == true){
                appTemplate.getGUI().enableHintButton(true);
            }else{
                // do nothing because the button is not used.
            }

            HBox remainingGuessBox = gameWorkspace.getRemainingGuessBox();
            remains = new Label(Integer.toString(gamedata.getRemainingGuesses( )));
            remainingGuessBox.getChildren().addAll(new Label("Remaining Guesses: "), remains);
            success = false;
            play();
        }catch (Exception e){
            PropertyManager           manager    = PropertyManager.getManager();
            AppMessageDialogSingleton dialog     = AppMessageDialogSingleton.getSingleton();
            dialog.show(manager.getPropertyValue("Error Loading File"), "Your File is corrupt Load a different File!!");
        }
    }


    private void restoreWordGraphics(VBox guessedLetters) {
        discovered = 0;
        Workspace gameWorkspace = (Workspace) appTemplate.getWorkspaceComponent();
        char[] targetword = gamedata.getTargetWord().toCharArray();
        checkingDifficultyHint();
        progress = new Text[targetword.length];

        guessLetterPane= new GridPane();
        GridPane pane= new GridPane();
        pane.setGridLinesVisible(true);
        pane.setStyle("-fx-padding:10; -fx-font-size: 25px");
        pane.setHgap(5);

        Set<Character> goodInput= new HashSet<>();

        for (int i = 0; i < progress.length; i++) {
            progress[i] = new Text(Character.toString(targetword[i]));

            StackPane paneHolder = new StackPane();
            paneHolder.setStyle("-fx-background-color: transparent");
            progress[i] = new Text(Character.toString(targetword[i]));
            Rectangle rect= new Rectangle(30,30);
            rect.setStyle("-fx-background-color: transparent");
            rect.setFill(Color.valueOf("WHITE"));
            paneHolder.getChildren().addAll(rect, progress[i]);
            progress[i].setVisible(false);
            pane.add(paneHolder, i , 0);


            progress[i].setVisible(gamedata.getGoodGuesses().contains(progress[i].getText().charAt(0)));
            if(gamedata.getGoodGuesses().contains(progress[i].getText().charAt(0))){
                if(goodInput.contains(progress[i].getText().charAt(0))) {
                }
                else {
                    goodInput.add(progress[i].getText().charAt(0));
                    gameWorkspace.changeColorIfGuess(progress[i].getText().charAt(0));
                    Text correctT= new Text(progress[i].getText()+ ", ");
                    guessLetterPane.add(correctT,goodGuess,0);          // added the good guess in the pane to display
                    goodGuess++;
                }
            }

            if (progress[i].isVisible()){
                discovered++;
            }
        }

        // Displaying the wrong message when the file is loaded
        for(char wrongChar: gamedata.getBadGuesses()){

            Text wrongT= new Text(wrongChar+ ", ");
            gameWorkspace.changeColorIfWrong(wrongChar);


            if(gamedata.getGoodGuesses().contains(wrongChar)) {
                // do nothing
            }
            else{
                guessLetterPane.add(wrongT,badGuess,1);
            }
            badGuess++;
            makeHangmanGraphics(badGuess);              // Draw the Hangman Graphics !
        }

        guessLetterPane.setStyle(" -fx-padding:15px; -fx-font-size: 15px");
        Label correctLabel= new Label("   Correct Guess : ");
        Label wrongLabel = new Label("   Wrong Guess  : ");
        Label difficultyLevel= new Label("   Level : ");
        HBox boxDifficultyLabel= new HBox(difficultyLevel,difficultyLevelString);
        guessLetterPane.add(correctLabel, 0 , 0);
        guessLetterPane.add(wrongLabel, 0 , 1);
        guessLetterPane.add(boxDifficultyLabel, 0, 2);
        guessedLetters.getChildren().addAll(pane, guessLetterPane);

    }

    private boolean alreadyGuessed(char c) {
        return gamedata.getGoodGuesses().contains(c) || gamedata.getBadGuesses().contains(c);
    }

    @Override
    public void handleNewRequest() {
        AppMessageDialogSingleton messageDialog   = AppMessageDialogSingleton.getSingleton();
        PropertyManager           propertyManager = PropertyManager.getManager();
        boolean                   makenew         = true;
        if (gamestate.equals(GameState.INITIALIZED_MODIFIED))
            try {
                makenew = promptToSave();
            } catch (IOException e) {
                messageDialog.show(propertyManager.getPropertyValue(NEW_ERROR_TITLE), propertyManager.getPropertyValue(NEW_ERROR_MESSAGE));
            }
        if (makenew) {
            appTemplate.getDataComponent().reset();                // reset the data (should be reflected in GUI)
            appTemplate.getWorkspaceComponent().reloadWorkspace(); // load data into workspace
            ensureActivatedWorkspace();                            // ensure workspace is activated
            workFile = null;                                       // new workspace has never been saved to a file
            ((Workspace) appTemplate.getWorkspaceComponent()).reinitialize();
            enableGameButton();
            appTemplate.getGUI().enableHintButton(true);
        }
        if (gamestate.equals(GameState.ENDED)) {
            appTemplate.getGUI().updateWorkspaceToolbar(false);
            Workspace gameWorkspace = (Workspace) appTemplate.getWorkspaceComponent();
            gameWorkspace.reinitialize();
        }

    }

    @Override
    public void handleSaveRequest() throws IOException {
        PropertyManager propertyManager = PropertyManager.getManager();
        if (workFile == null) {
            FileChooser filechooser = new FileChooser();
            Path        appDirPath  = Paths.get(propertyManager.getPropertyValue(APP_TITLE)).toAbsolutePath();
            Path        targetPath  = appDirPath.resolve(APP_WORKDIR_PATH.getParameter());
            filechooser.setInitialDirectory(targetPath.toFile());
            filechooser.setTitle(propertyManager.getPropertyValue(SAVE_WORK_TITLE));
            String description = propertyManager.getPropertyValue(WORK_FILE_EXT_DESC);
            String extension   = propertyManager.getPropertyValue(WORK_FILE_EXT);
            ExtensionFilter extFilter = new ExtensionFilter(String.format("%s (*.%s)", description, extension),
                                                            String.format("*.%s", extension));
            filechooser.getExtensionFilters().add(extFilter);
            File selectedFile = filechooser.showSaveDialog(appTemplate.getGUI().getWindow());
            if (selectedFile != null)
                save(selectedFile.toPath());
        } else
            save(workFile);
    }

    @Override
    public void handleLoadRequest() throws IOException {
        boolean load = true;
        flagLoadSave= true;
        if (gamestate.equals(GameState.INITIALIZED_MODIFIED))
            load = promptToSave();
        if (load) {
            PropertyManager propertyManager = PropertyManager.getManager();
            FileChooser filechooser = new FileChooser();
            Path appDirPath = Paths.get(propertyManager.getPropertyValue(APP_TITLE)).toAbsolutePath();
            Path targetPath = appDirPath.resolve(APP_WORKDIR_PATH.getParameter());
            filechooser.setInitialDirectory(targetPath.toFile());
            filechooser.setTitle(propertyManager.getPropertyValue(LOAD_WORK_TITLE));
            String description = propertyManager.getPropertyValue(WORK_FILE_EXT_DESC);
            String extension = propertyManager.getPropertyValue(WORK_FILE_EXT);
            ExtensionFilter extFilter = new ExtensionFilter(String.format("%s (*.%s)", description, extension),
                    String.format("*.%s", extension));
            filechooser.getExtensionFilters().add(extFilter);
            File selectedFile = filechooser.showOpenDialog(appTemplate.getGUI().getWindow());
            if (selectedFile != null && selectedFile.exists()) {
                load(selectedFile.toPath());
                restoreGUI(); // restores the GUI to reflect the state in which the loaded game was last saved
            }
        }
    }

    @Override
    public void handleExitRequest() {
        try {
            boolean exit = true;
            if (gamestate.equals(GameState.INITIALIZED_MODIFIED))
                exit = promptToSave();
            if (exit)
                System.exit(0);
        } catch (IOException ioe) {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            PropertyManager           props  = PropertyManager.getManager();
            dialog.show(props.getPropertyValue(SAVE_ERROR_TITLE), props.getPropertyValue(SAVE_ERROR_MESSAGE));
        }
    }

    private void ensureActivatedWorkspace() {
        appTemplate.getWorkspaceComponent().activateWorkspace(appTemplate.getGUI().getAppPane());
    }

    private boolean promptToSave() throws IOException {
        PropertyManager            propertyManager   = PropertyManager.getManager();
        YesNoCancelDialogSingleton yesNoCancelDialog = YesNoCancelDialogSingleton.getSingleton();

        yesNoCancelDialog.show(propertyManager.getPropertyValue(SAVE_UNSAVED_WORK_TITLE),
                               propertyManager.getPropertyValue(SAVE_UNSAVED_WORK_MESSAGE));

        if (yesNoCancelDialog.getSelection().equals(YesNoCancelDialogSingleton.YES))
            handleSaveRequest();

        return !yesNoCancelDialog.getSelection().equals(YesNoCancelDialogSingleton.CANCEL);
    }

    /**
     * A helper method to save work. It saves the work, marks the current work file as saved, notifies the user, and
     * updates the appropriate controls in the user interface
     *
     * @param target The file to which the work will be saved.
     * @throws IOException
     */
    private void save(Path target) throws IOException {
        appTemplate.getFileComponent().saveData(appTemplate.getDataComponent(), target);
        workFile = target;
        setGameState(GameState.INITIALIZED_UNMODIFIED);
        AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
        PropertyManager           props  = PropertyManager.getManager();
        dialog.show(props.getPropertyValue(SAVE_COMPLETED_TITLE), props.getPropertyValue(SAVE_COMPLETED_MESSAGE));

    }

    /**
     * A helper method to load saved game data. It loads the game data, notified the user, and then updates the GUI to
     * reflect the correct state of the game.
     *
     * @param source The source data file from which the game is loaded.
     * @throws IOException
     */
    private void load(Path source) throws IOException {
        // load game data
        appTemplate.getFileComponent().loadData(appTemplate.getDataComponent(), source);

        // set the work file as the file from which the game was loaded
        workFile = source;

        // notify the user that load was successful
        AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
        PropertyManager           props  = PropertyManager.getManager();
        dialog.show(props.getPropertyValue(LOAD_COMPLETED_TITLE), props.getPropertyValue(LOAD_COMPLETED_MESSAGE));

        setGameState(GameState.INITIALIZED_UNMODIFIED);
        Workspace gameworkspace = (Workspace) appTemplate.getWorkspaceComponent();
        ensureActivatedWorkspace();
        gameworkspace.reinitialize();
        gamedata = (GameData) appTemplate.getDataComponent();

    }
}
