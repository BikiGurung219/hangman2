package gui;

import apptemplate.AppTemplate;
import components.AppWorkspaceComponent;
import controller.HangmanController;
import javafx.collections.ObservableList;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ToolBar;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.shape.ArcType;
import javafx.scene.shape.Rectangle;
import org.w3c.dom.css.Rect;
import propertymanager.PropertyManager;
import ui.AppGUI;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Queue;
import java.util.Set;

import static hangman.HangmanProperties.*;

/**
 * This class serves as the GUI component for the Hangman game.
 *
 * @author Ritwik Banerjee
 */
public class Workspace extends AppWorkspaceComponent {

    AppTemplate app; // the actual application
    AppGUI      gui; // the GUI inside which the application sits

    Label             guiHeadingLabel;   // workspace (GUI) heading label
    HBox              headPane;          // conatainer to display the heading
    VBox              bodyPane;          // container for the main game displays
    ToolBar           footToolbar;       // toolbar for game buttons
    BorderPane        figurePane;        // container to display the namesake graphic of the (potentially) hanging person
    VBox               gameTextsPane;     // container to display the text-related parts of the game
    VBox              guessedLetters;    // text area displaying all the letters guessed so far
    HBox              remainingGuessBox; // container to display the number of remaining guesses
    HBox              hangmanGraphicsPane;
    Button            startGame;         // the button to start playing a game of Hangman
    HangmanController controller;
    Canvas canvas;
    GraphicsContext gc;
    GridPane gpane;
    StackPane[] pane;

    /**
     * Constructor for initializing the workspace, note that this constructor
     * will fully setup the workspace user interface for use.
     *
     * @param initApp The application this workspace is part of.
     * @throws IOException Thrown should there be an error loading application
     *                     data for setting up the user interface.
     */
    public Workspace(AppTemplate initApp) throws IOException {
        app = initApp;
        gui = app.getGUI();
        controller = (HangmanController) gui.getFileController();    //new HangmanController(app, startGame); <-- THIS WAS A MAJOR BUG!??
        layoutGUI();     // initialize all the workspace (GUI) components including the containers and their layout
        setupHandlers(); // ... and set up event handling
    }

    private void layoutGUI() {
        PropertyManager propertyManager = PropertyManager.getManager();
        guiHeadingLabel = new Label(propertyManager.getPropertyValue(WORKSPACE_HEADING_LABEL));

        headPane = new HBox();
        headPane.getChildren().add(guiHeadingLabel);
        headPane.setAlignment(Pos.CENTER);

        figurePane = new BorderPane();
        guessedLetters = new VBox();
        guessedLetters.setStyle("-fx-background-color: transparent;");
        remainingGuessBox = new HBox();
        gameTextsPane = new VBox();
        gameTextsPane.getChildren().setAll(guessedLetters,remainingGuessBox );

        bodyPane = new VBox();

        hangmanGraphicsPane= new HBox();
        double h=400;
        double w=500;
        canvas = new Canvas(h,w);
        gc = canvas.getGraphicsContext2D();

        hangmanGraphicsPane.getChildren().addAll(canvas);

        bodyPane.getChildren().addAll(gameTextsPane);
        startGame = new Button("Start Playing");
        HBox blankBoxLeft  = new HBox();
        HBox blankBoxRight = new HBox();
        HBox.setHgrow(blankBoxLeft, Priority.ALWAYS);
        HBox.setHgrow(blankBoxRight, Priority.ALWAYS);
        footToolbar = new ToolBar(blankBoxLeft, startGame, blankBoxRight);

        workspace = new VBox();
        workspace.getChildren().addAll(headPane, footToolbar,bodyPane, hangmanGraphicsPane);
    }

    private void setupHandlers() {
        startGame.setOnMouseClicked(e -> controller.start());
    }

    /**
     * This function specifies the CSS for all the UI components known at the time the workspace is initially
     * constructed. Components added and/or removed dynamically as the application runs need to be set up separately.
     */
    @Override
    public void initStyle() {
        PropertyManager propertyManager = PropertyManager.getManager();

        gui.getAppPane().setId(propertyManager.getPropertyValue(ROOT_BORDERPANE_ID));
        gui.getToolbarPane().getStyleClass().setAll(propertyManager.getPropertyValue(SEGMENTED_BUTTON_BAR));
        gui.getToolbarPane().setId(propertyManager.getPropertyValue(TOP_TOOLBAR_ID));

        ObservableList<Node> toolbarChildren = gui.getToolbarPane().getChildren();
        toolbarChildren.get(0).getStyleClass().add(propertyManager.getPropertyValue(FIRST_TOOLBAR_BUTTON));
        toolbarChildren.get(toolbarChildren.size() - 1).getStyleClass().add(propertyManager.getPropertyValue(LAST_TOOLBAR_BUTTON));

        workspace.getStyleClass().add(CLASS_BORDERED_PANE);
        guiHeadingLabel.getStyleClass().setAll(propertyManager.getPropertyValue(HEADING_LABEL));
    }

    /** This function reloads the entire workspace */
    @Override
    public void reloadWorkspace() {
        /* does nothing; use reinitialize() instead */
    }

    public HBox getHangmanGraphicsPane(){
        return hangmanGraphicsPane;
    }

    public VBox getGameTextsPane() {
        return gameTextsPane;
    }

    public HBox getRemainingGuessBox() {
        return remainingGuessBox;
    }

    public Button getStartGame() {
        return startGame;
    }

    public GraphicsContext getCanvas(){
        return gc;
    }

    public void reinitialize() {
        app.getGUI().hideHintButton();
        guessedLetters = new VBox();
        guessedLetters.setStyle("-fx-background-color: transparent;");
        remainingGuessBox = new HBox();
        gameTextsPane = new VBox();
        gameTextsPane.getChildren().setAll(remainingGuessBox, guessedLetters);
        hangmanGraphicsPane= new HBox();
        double h=400;
        double w=500;
        canvas = new Canvas(h,w);
        gc = canvas.getGraphicsContext2D();
        hangmanGraphicsPane.getChildren().addAll(canvas);
        bodyPane.getChildren().setAll(figurePane, gameTextsPane,hangmanGraphicsPane);
    }

    /**
     * This method helps to generate the words from A-Z in the pane with different properties like
     * the rectangle and the label.
     * @Author BG
     * @return Gridpane
     */

    public GridPane generateLetterHeadings(){
        char initialA= 'a';
        gpane= new GridPane();
        gpane.setStyle("-fx-padding:15; -fx-font-size: 30px");
        gpane.setHgap(3);
        gpane.setGridLinesVisible(true);
        pane= new StackPane[26];
        for(int i=0;i<26;i++){
            pane[i]= new StackPane();
            pane[i].setStyle("-fx-background-color: transparent");
            Rectangle rect= new Rectangle(33,33);
            rect.setStyle("-fx-background-color: transparent");
            rect.setFill(Color.valueOf("blue"));
            Label labelLetters= new Label(initialA+"");
            pane[i].getChildren().addAll(rect, labelLetters);
            gpane.add(pane[i],i,0);
            initialA= (char) ((int)initialA + 1);
        }
        return gpane;
    }

    public void changeColorIfGuess(char goodGuess){
        int num= Character.toLowerCase(goodGuess);
        int position= num - 'a';
        Label checkL= new Label(goodGuess+"");
        Rectangle rect= new Rectangle(33,33);
        rect.setStyle("-fx-background-color: transparent");
        rect.setFill(Color.valueOf("white"));
        pane[position].getChildren().addAll(rect,checkL);
    }

    public void changeColorIfWrong(char wrongGuess){
        int num= Character.toLowerCase(wrongGuess);
        int position= num - 'a';
        Label checkL= new Label(wrongGuess+"");
        Rectangle rect= new Rectangle(33,33);
        rect.setStyle("-fx-background-color: transparent");
        rect.setFill(Color.valueOf("red"));
        pane[position].getChildren().addAll(rect,checkL);
    }

}

