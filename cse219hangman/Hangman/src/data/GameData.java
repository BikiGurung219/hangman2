package data;

import apptemplate.AppTemplate;
import components.AppDataComponent;
import controller.GameError;

import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;
import java.util.stream.Stream;

/**
 * @author Ritwik Banerjee
 */
public class GameData implements AppDataComponent {

    public static final  int TOTAL_NUMBER_OF_GUESSES_ALLOWED = 10;
    private static final int TOTAL_NUMBER_OF_STORED_WORDS    = 330622;

    private String         targetWord;
    private Set<Character> goodGuesses;
    private Set<Character> badGuesses;
    private int            remainingGuesses;
    public  AppTemplate    appTemplate;
    private String correctWord;
    private boolean hintUsed = false;

    public GameData(AppTemplate appTemplate) {
        this(appTemplate, false);
    }

    public GameData(AppTemplate appTemplate, boolean initiateGame) {
        if (initiateGame) {
            this.appTemplate = appTemplate;
            this.targetWord = checkString();
            this.goodGuesses = new HashSet<>();
            this.badGuesses = new HashSet<>();
            this.remainingGuesses = TOTAL_NUMBER_OF_GUESSES_ALLOWED;
        } else {
            this.appTemplate = appTemplate;
        }
    }

    public void init() {
        this.targetWord = checkString();
        this.goodGuesses = new HashSet<>();
        this.badGuesses = new HashSet<>();
        this.remainingGuesses = TOTAL_NUMBER_OF_GUESSES_ALLOWED;
    }



    /**
     * The gandata has the method to check the level of Difficulty so that the user knows the level of dufficulty to play or to
     * exit or to continue
     * @return String : The Level of difficiulty
     * @Author: BG
     */
    public String checkDifficulty(){
        String wordToCheck= targetWord;
        int counter = 0;
        Set<Character> checkStrng= new HashSet<>();

        for(int i = 0;i < wordToCheck.length();i++){
            if(checkStrng.contains(wordToCheck.charAt(i))){
                //do nothing
            }
            else{
                checkStrng.add(wordToCheck.charAt(i));
                counter ++;
            }
        }
        if(counter > 7){
           // char randomChar=  printHintWord();

            return "Difficult";
        }
        else{
            return "Easy";
        }
    }

    /**
     * This method helps the user to find if the number generated randomly has already been guessed by the user
     * so it helps to generate a unique character from the guess
     * @return character i.e the uniqe char
     * @Author BG
     */

    public char checkIfGoodGuess(){
        char randomChar=  printHintWord();
        while(goodGuesses.contains(randomChar)){
            randomChar = printHintWord();
        }
        return randomChar;
    }

    /**
     * To check if the Hint was previously used then the user should not be allowed to use the hint again
     * @return
     */
    public boolean hintUsedChecking(boolean used){
        hintUsed = used;
        return hintUsed;
    }

    public boolean getHintUsedCheck(){
        return hintUsed;
    }


    public char printHintWord(){
        Random rand= new Random();
        int randomString=rand.nextInt(targetWord.length());
        char wordFromTarget= targetWord.charAt(randomString);
        return wordFromTarget;
    }

    /**
     * Checking if the given word is valid or not using recursion
     * @author BG
     * @return String the correct Word
     */

    public String checkString() {
        String wordStringChoice= setTargetWord().toLowerCase();
        char[] newStringWord= wordStringChoice.toCharArray();
        correctWord=null;
        boolean flag= true;

        for (char checkTargetWord : newStringWord) {
            if (checkTargetWord >= 'a' && checkTargetWord <= 'z') {
                //do nothing
            } else {
                flag = false;
                break;
            }
        }

        if(flag){
            correctWord = wordStringChoice;
            return correctWord;
        }
        else{
            correctWord = checkString();    // Using recursion to load the incorrect string again
            return correctWord;
        }

    }

    @Override
    public void reset() {
        this.targetWord = null;
        this.goodGuesses = new HashSet<>();
        this.badGuesses = new HashSet<>();
        this.remainingGuesses = TOTAL_NUMBER_OF_GUESSES_ALLOWED;
        appTemplate.getWorkspaceComponent().reloadWorkspace();
    }

    public String getTargetWord() {
        return targetWord;
    }

    private String setTargetWord() {
        URL wordsResource = getClass().getClassLoader().getResource("words/words.txt");
        assert wordsResource != null;

        int toSkip = new Random().nextInt(TOTAL_NUMBER_OF_STORED_WORDS);

        try (Stream<String> lines = Files.lines(Paths.get(wordsResource.toURI()))) {
            return lines.skip(toSkip).findFirst().get();
        } catch (IOException | URISyntaxException e) {
            e.printStackTrace();
            System.exit(1);
        }

        throw new GameError("Unable to load initial target word.");
    }

    public GameData setTargetWord(String targetWord) {
        this.targetWord = targetWord;
        return this;
    }

    public Set<Character> getGoodGuesses() {
        return goodGuesses;
    }

    public GameData setGoodGuesses(Set<Character> goodGuesses) {
        this.goodGuesses = goodGuesses;
        return this;
    }

    public Set<Character> getBadGuesses() {
        return badGuesses;
    }

    public GameData setBadGuesses(Set<Character> badGuesses) {
        this.badGuesses = badGuesses;
        return this;
    }

    public int getRemainingGuesses() {
        return remainingGuesses;
    }

    public void addGoodGuess(char c) {
        goodGuesses.add(c);
    }

    public void addBadGuess(char c) {
        if (!badGuesses.contains(c)) {
            badGuesses.add(c);
            remainingGuesses--;
        }
    }


}
